/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */
package com.taobao.dingtalk.spring.annotations;

import com.taobao.dingtalk.spring.configuration.DingTalkConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 应用钉钉开放平台
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import(DingTalkConfiguration.class)
public @interface EnableDingTalk {

}
