/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.taobao.dingtalk.model.message;

import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.Map;

/***
 *
 * @since:dingtalk-api 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2019/11/28 10:39
 */
public class DingMessageVoice extends DingMessage {

    private final String mediaId;
    private final Integer duration;

    /**
     * 语音消息
     * @param mediaId 媒体文件id。2MB，播放长度不超过60s，AMR格式
     * @param duration 正整数，小于60，表示音频时长
     */
    public DingMessageVoice(String mediaId, Integer duration) {
        super(MESSAGE_VOICE);
        if (duration==null||duration>60){
            throw new IllegalArgumentException("音频时长必须小于60");
        }
        this.mediaId = mediaId;
        this.duration = duration;
    }

    @Override
    public String toString() {
        //得到文本消息类JSON字符串
        Map<String,Object> map=getDefaultMap();
        Map<String,Object> voice=new HashMap<>();
        voice.put("media_id",mediaId);
        voice.put("duration",duration.toString());
        map.put("voice",voice);
        return JSON.toJSONString(map);
    }
}
